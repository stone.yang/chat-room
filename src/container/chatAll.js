import { connect } from 'react-redux'
import ChatAll from '../components/chatall'
import {message_update, game_current} from '../action'
import { hashHistory } from 'react-router'
import SessionService from "../services/session";


var mapStateToProps = (state, ownProps) => {
  return {
    nickName: state.nickName,
    msgList: state.msgList,
    count: state.count,
    games: state.games,
    game: state.game,
  }
}

var mapDispatchToProps = (dispatch, ownProps) => {
  return {
    handleClick: function(e) {
      //todo 登出
      SessionService.removeToken();
      hashHistory.push('/login');

      // fetch('/api/logout', {
      //   method: 'POST',
      //   body: '',
      //   credentials: 'include'
      // }).then(function(res) {
      //   if (res.ok) {
      //     hashHistory.push('/login');
      //     dispatch(nickname_forget());
      //   }
      // });
    },
    handleSubmit: function(value, gameId) {
      //todo 傳送訊息至聊天室 {"type":"pushChatRoom","roomId":"abc","msgType":1,"msg":"hello"}
      dispatch(message_update({
        type: "pushChatRoom",
        roomId: gameId.toString(),
        msgType: 1,
        msg: value
      }));
      // dispatch(message_update({
      //   nickName: nickName,
      //   msg: value
      // }));
    },
    checkLogin: function() {
      //todo 驗證
      const token = SessionService.getToken();
      if (token === null){
        hashHistory.push('/login');
      }
      // fetch('/api/auth', {
      //   method: 'GET',
      //   credentials: 'include'
      // }).then(function(res) {
      //   return res.json()
      // }).then(function(data) {
      //   //如果没有cookie，则证明没有登录过，重定向到昵称页面
      //   if (!data.permit) {
      //     hashHistory.push('/login');
      //   } else {
      //     dispatch(nickname_get(data.nickname));
      //   }
      // })
    },
    selectGame: function(game) {
      dispatch(game_current(game));
    }
  }
}

var ChatAllContainer = connect(mapStateToProps, mapDispatchToProps)(ChatAll);
export default ChatAllContainer
