function message_update(msg) {
  return {
    type: 'MSG_UPDATE',
    msg
  }
}

function nickname_get(nickName) {
  return {
    type: 'NICKNAME_GET',
    nickName
  }
}

function message_clean() {
  return {
    type: 'MSG_CLEAN'
  }
}

function conn_get(host) {
  return {
    type: 'WS_CONNECT',
    host
  }
}

function message_get(getmsg) {
  return {
    type: 'MSG_GET',
    getmsg
  }
}

function count_update(count) {
  return {
    type: 'COUNT_UPDATE',
    count
  }
}

// todo 聊天室
function games_get(games) {
  return {
    type: 'GAMES_GET',
    games
  }
}

function game_current(game) {
  return {
    type: 'GAME_CURRENT',
    game
  }
}

export { message_update, nickname_get, conn_get, message_get, count_update, games_get, game_current, message_clean}
