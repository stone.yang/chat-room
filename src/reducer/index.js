import { combineReducers } from 'redux'

function nickname_reducer(state = '', action) {
  switch (action.type) {
    case 'NICKNAME_GET':
      return action.nickName;
    default:
      return state;
  }
}

function msg_reducer(state = [], action) {
  switch (action.type) {
    case 'MSG_GET':
      var newState = [];
      newState = [...state, action.getmsg];
      return newState;
    case 'MSG_CLEAN':
      return [];
    default:
      return state;
  }
}

function conn_reducer(state = '', action) {
  switch (action.type) {
    case 'WS_CONNECT':
      return action.host;
    default:
      return state;
  }
}

function count_reducer(state =0, action) {
  switch (action.type) {
    case 'COUNT_UPDATE':
      return action.count;
    default:
      return state;
  }
}

function games_reducer(state = [], action) {
  switch (action.type) {
    case 'GAMES_GET':
      return action.games;
    default:
      return state;
  }
}

function game_reducer(state ={}, action) {
  switch (action.type) {
    case 'GAME_CURRENT':
      return action.game;
    default:
      return state;
  }
}

var reducers = combineReducers({
  nickName: nickname_reducer,
  msgList: msg_reducer,
  conn: conn_reducer,
  count: count_reducer,
  games: games_reducer,
  game: game_reducer,
});

export default reducers
