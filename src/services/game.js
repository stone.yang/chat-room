export default class GameService {
    static getGamesAPI() {
        return fetch('https://tstgendev.tg7777.net/relayApi/api/games/leagues/all/live_streaming_list', {
            method: 'GET',
            headers: {
                'Platform':'Web_1.0'
            }
        }).then(function(res) {
            return res.json();
        }).then(function(data) {
            return data;
        })
    }
    static async getGames() {
        const data = await this.getGamesAPI();

        const arr = [];
        for (let i = 0; i < data.Payload.length ; i++) {
            arr.push(data.Payload[i]);
            if (i == 9){
                break
            }
        }
        return arr;
        // return data.Payload; // leagueName, animation, matchId (int)
    }
}