import React from 'react'
// import ReactPlayer from 'react-player'



require('./index.less');

class Game extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
        <div className="game">
          <iframe width="800" height="450" src={this.props.game.animation} frameBorder="0"
                  allowFullScreen></iframe>
        </div>
    )
  }
}

export default Game
