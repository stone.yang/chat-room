import React from 'react'
import TypeIn from '../typein/index.js'
import MsgShow from '../msgshow/index.js'
import Nav from '../nav/index.js'
import GameList from '../gamelist/index.js'
import Game from '../game/index.js'
import SessionService from "../../services/session";

require('./index.less');

class ChatAll extends React.Component {
  constructor(props) {
    super(props);
    window.onunload = () => {
      // Clear the local storage
      SessionService.removeToken();
    }
  }

  componentWillMount() {
    this.props.checkLogin();
  }

  render() {
    const {handleClick, msgList, nickName, handleSubmit, count, games, game, selectGame} = this.props;
    if (nickName == '') {
      return null;
    } else {
      return (
        <div className='chat-wrap'>
          <Nav handleClick={handleClick} nickName={nickName}/>
          <div className='message-wrap'>
            <GameList selectGame={selectGame} games={games} />
            <Game game={game} />
            <div className='typein-wrap'>
              <MsgShow msgList={msgList} count={count} />
              <TypeIn handleSubmit={handleSubmit} game={game}/>
            </div>
          </div>
        </div>
      )
    }
  }
}

export default ChatAll
