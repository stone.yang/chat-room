import React from 'react'

require('./index.less');

class GameList extends React.Component {
  constructor(props) {
      super(props);
  }
  changeGameIndex(index) {
      this.props.selectGame(this.props.games[index]);
  }
  render() {
    return (
      <div className='game-list'>
        <h5>足球賽事</h5>
        <ul style={{margin:"10px"}}>
          {this.props.games.map((item, index) => (
              <li className='name-list-title'>
                  <a key={index} href="#" onClick={() => this.changeGameIndex(index)}>{item.leagueName}</a>
              </li >
          ))}
        </ul>
      </div>
    )
  }
}

export default GameList
